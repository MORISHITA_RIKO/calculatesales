package jp.alhinc.morishita_riko.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}
		System.out.println("ここにあるファイルを開きます => " + args[0]);

		Map<String, String> branchNames = new HashMap<String, String>();
		Map<String, Long> branchSales = new HashMap<String, Long>();

		Map<String, String>commodityNames = new HashMap<String, String>();
		Map<String, Long>commoditySales = new HashMap<String, Long>();

		if(!branchfileInput(args[0], branchNames, branchSales, "branch.lst", "支店","[0-9]{3}")) {
			return;
		}
		if(!commodityfileInput(args[0], commodityNames, commoditySales, "commodity.lst", "商品", "[a-zA-Z0-9]{8}")) {
			return;
		}
		//売上ファイルの判定・読み込み
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length ; i++) {
			if(files[i].isFile() && files[i].getName().matches("[0-9]{8}.+rcd$")){
				rcdFiles.add(files[i]);
			}
		}
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			Collections.sort(rcdFiles);
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));

			if((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません。");
				return;
			}
		}
		for(int i = 0; i < rcdFiles.size() ; i++) {
			BufferedReader salesFileBr = null;
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				salesFileBr = new BufferedReader(fr);

				String line;
				List<String> salesFiles = new ArrayList<>();
				while((line = salesFileBr.readLine()) != null) {
					salesFiles.add(line);
				}
				System.out.println(salesFiles);

				if(salesFiles.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です。");
					return;
				}
				if(!branchNames.containsKey(salesFiles.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です。");
					return;
				}
				if(!commodityNames.containsKey(salesFiles.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です。");
					return;
				}
				if(!salesFiles.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました。");
					return;
				}
				long fileSale = Long.parseLong(salesFiles.get(2));
				long saleAmount = branchSales.get(salesFiles.get(0)) + fileSale;
				long commoditySaleAmount = commoditySales.get(salesFiles.get(1)) + fileSale;

				if(saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました。");
					return;
				}

				if(commoditySaleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました。");
					return;
				}
				branchSales.put(salesFiles.get(0), saleAmount);
				commoditySales.put(salesFiles.get(1), commoditySaleAmount);

			} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
			} finally {
					if(salesFileBr != null) {
						try {
							salesFileBr.close();
						} catch(IOException e) {
							System.out.println("予期せぬエラーが発生しました。");
						}
					}
			}
		}
		if(!branchfileOutput(args[0], branchNames, branchSales, "branch.out")) {
			return;
		}
		if(!commodityfileOutput(args[0], commodityNames, commoditySales, "commodity.out")) {
			return;
		}
	}
	//支店定義ファイルの読み込み
	private static boolean branchfileInput(String path, Map<String, String> namesMap, Map<String, Long> salesMap, String fileName, String fileType, String regex) {
		BufferedReader br = null;
		try {
			File file = new File(path,fileName);
			if(!file.exists()) {
				System.out.println(fileType + "定義ファイルが存在しません。");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

				if(items.length != 2 || ! items[0].matches(regex)) {
					System.out.println(fileType + "定義ファイルのフォーマットが不正です。");
					return false;
				}
				namesMap.put(items[0], items[1]);
				salesMap.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}
	//商品定義ファイルの読み込み
	private static boolean commodityfileInput(String path, Map<String, String> namesMap, Map<String, Long> salesMap, String fileName, String fileType, String regex) {
		BufferedReader br = null;
		try {
			File file = new File(path,fileName);
			if(!file.exists()) {
				System.out.println(fileType + "定義ファイルが存在しません。");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

				if(items.length != 2 || ! items[0].matches(regex)) {
					System.out.println(fileType + "定義ファイルのフォーマットが不正です。");
					return false;
				}
				namesMap.put(items[0], items[1]);
				salesMap.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}
	//支店別集計ファイルの作成
	private static boolean branchfileOutput(String path, Map<String, String> namesMap, Map<String, Long> salesMap, String fileName) {
		BufferedWriter bw = null;
		try {
			File file = new File(path,fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : salesMap.keySet()) {
				bw.write(key + "," + namesMap.get(key) + "," + salesMap.get(key));
				bw.newLine();
			}
		} catch(IOException	e) {
			System.out.println("予期せぬエラーが発生しました。");
			System.out.println(e);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}
	//商品別集計ファイル
	private static boolean commodityfileOutput(String path, Map<String, String> namesMap, Map<String, Long> salesMap, String fileName) {
		BufferedWriter bw = null;
		try {
			File file = new File(path,fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : salesMap.keySet()) {
				bw.write(key + "," + namesMap.get(key) + "," + salesMap.get(key));
				bw.newLine();
			}
		} catch(IOException	e) {
			System.out.println("予期せぬエラーが発生しました。");
			System.out.println(e);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}
}
